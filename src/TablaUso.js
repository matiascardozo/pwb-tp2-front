import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
});


function TablaUso(props) {
  const { classes, datos } = props;

  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell align="right">Id</TableCell>
            <TableCell align="right">Puntaje Utilizado</TableCell>
            <TableCell align="right">Fecha</TableCell>
            <TableCell align="right">Concepto</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {datos.map(dato => (
            <TableRow key={dato.id}>
              
              <TableCell align="right">{dato.id}</TableCell>
              <TableCell align="right">{dato.puntosUtilizados}</TableCell>
              <TableCell align="right">{dato.fecha}</TableCell>
              <TableCell align="right">{dato.Concepto.descripcion}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
  );
}

TablaUso.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TablaUso);