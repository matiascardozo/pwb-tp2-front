import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import blue from "@material-ui/core/colors/blue";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import MuiDialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemText from "@material-ui/core/ListItemText";
import Paper from "@material-ui/core/Paper";
import { withStyles } from "@material-ui/core/styles";
import { emphasize } from "@material-ui/core/styles/colorManipulator";
import PersonIcon from "@material-ui/icons/Person";
import axios from "axios";
import PropTypes from "prop-types";
import React from "react";
import deburr from "lodash/deburr";
import Downshift from "downshift";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";

const DialogContent = withStyles(theme => ({
  root: {
    margin: 0,
    padding: theme.spacing.unit * 2,
    height: "350px"
  }
}))(MuiDialogContent);

const clientes = [{ id: 1, ci: 4242424 }, { id: 2, ci: 5552214 }];

const styles = theme => ({
  avatar: {
    backgroundColor: blue[100],
    color: blue[600]
  },

  containerDiv: {
    display: "flex",

    justifyContent: "space-around",
    flexFlow: "row wrap",
    alignItems: "stretch"
  },

  container: {
    flexGrow: 1,
    position: "relative"
  },
  root: {
    flexGrow: 1,
    height: 500
  },
  input: {
    display: "flex",
    padding: 0
  },
  valueContainer: {
    display: "flex",
    flexWrap: "wrap",
    flex: 1,
    alignItems: "center",
    overflow: "hidden"
  },
  chip: {
    margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`
  },
  chipFocused: {
    backgroundColor: emphasize(
      theme.palette.type === "light"
        ? theme.palette.grey[300]
        : theme.palette.grey[700],
      0.08
    )
  },
  noOptionsMessage: {
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`
  },
  singleValue: {
    fontSize: 16
  },
  placeholder: {
    position: "absolute",
    left: 2,
    fontSize: 16
  },
  paper: {
    position: "absolute",
    zIndex: 1,
    marginTop: theme.spacing.unit,
    left: 0,
    right: 0
  },
  divider: {
    height: theme.spacing.unit * 2
  }
});

function getSuggestions({ value, suggestions, key }) {
  const inputValue = deburr(value.trim()).toLowerCase();
  const inputLength = inputValue.length;
  let count = 0;

  return inputLength === 0
    ? []
    : suggestions.filter(suggestion => {
        const keep =
          count < 5 &&
          suggestion[key].slice(0, inputLength).toLowerCase() === inputValue;

        if (keep) {
          count += 1;
        }

        return keep;
      });
}

class DialogCliente extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      clientes: [],
      selectedCliente: {
        nroDocumento:"",
        nombre:"",
        apellido:""
      }
    };
  }

  componentDidMount() {
    axios
      .get("/cliente")
      .then(response =>
        this.setState({ clientes: response.data, loading: false })
      )
      .catch(error => this.setState({ error, loading: false }));
  }

  onInputValueChange = (inputValue, stateAndHelpers) => {
    const { selectedCliente } = this.state;
    const newSelected = { ...selectedCliente };
    newSelected[stateAndHelpers.id] = inputValue;
    this.setState({ selectedCliente: newSelected });
  };

  handleClose = () => {
    this.props.onClose(this.state.selectedCliente);
  };

  handleListItemClick = value => {
    this.props.onClose(value);
  };

  renderDialogViejo() {
    const { classes, onClose, selectedValue, ...other } = this.props;

    return (
      <Dialog
        onClose={this.handleClose}
        aria-labelledby="simple-dialog-title"
        {...other}
      >
        <DialogTitle id="simple-dialog-title">Seleccionar Cliente</DialogTitle>
        <div>
          <List>
            {this.state.clientes.map(cliente => (
              <ListItem
                button
                onClick={() => this.handleListItemClick(cliente)}
                key={cliente.id}
              >
                <ListItemAvatar>
                  <Avatar className={classes.avatar}>
                    <PersonIcon />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText primary={cliente.nroDocumento} />
              </ListItem>
            ))}
          </List>
        </div>
      </Dialog>
    );
  }

  handleSelect = (value, stateAndHelpers) => {
    console.log(stateAndHelpers);
    this.setState(prevState => ({
      selectedCliente: prevState.clientes.find(
        c => c[stateAndHelpers.id] === value
      )
    }));
  };

  render() {
    const { classes, onClose, selectedValue, ...other } = this.props;
    const { clientes } = this.state;
    return (
      <div>
        <Dialog
          open={this.props.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
          fullWidth={true}
          maxWidth={"xl"}
        >
          <DialogTitle id="form-dialog-title">Elegir Cliente</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Busca alguno de los campos para elegir al cliente
            </DialogContentText>

            <div className={classes.containerDiv}>
              <Downshift
                id="nroDocumento"
                name="nroDocumento"
                onChange={(selection, stateAndHelpers) =>
                  this.handleSelect(selection, stateAndHelpers)
                }
                inputValue={
                  this.state.selectedCliente
                    ? this.state.selectedCliente.nroDocumento
                    : ""
                }
                onInputValueChange={this.onInputValueChange}
              >
                {({
                  getInputProps,
                  getItemProps,
                  getMenuProps,
                  highlightedIndex,
                  inputValue,
                  isOpen,
                  selectedItem
                }) => (
                  <div className={classes.container}>
                    {renderInput({
                      classes,
                      InputProps: getInputProps({
                        placeholder: "Documento",
                        name: "nroDocumento"
                      })
                    })}
                    <div {...getMenuProps()}>
                      {isOpen ? (
                        <Paper className={classes.paper} square>
                          {getSuggestions({
                            value: inputValue,
                            suggestions: clientes,
                            key: "nroDocumento"
                          }).map((suggestion, index) =>
                            renderSuggestion({
                              suggestion,
                              index,
                              itemProps: getItemProps({
                                item: suggestion.nroDocumento
                              }),
                              highlightedIndex,
                              selectedItem,
                              key: "nroDocumento"
                            })
                          )}
                        </Paper>
                      ) : null}
                    </div>
                  </div>
                )}
              </Downshift>

              <Downshift
                id="nombre"
                onChange={(selection, stateAndHelpers) =>
                  this.handleSelect(selection, stateAndHelpers)
                }
                inputValue={
                  this.state.selectedCliente
                    ? this.state.selectedCliente.nombre
                    : ""
                }
                onInputValueChange={this.onInputValueChange}
              >
                {({
                  getInputProps,
                  getItemProps,
                  getMenuProps,
                  highlightedIndex,
                  inputValue,
                  isOpen,
                  selectedItem
                }) => (
                  <div className={classes.container}>
                    {renderInput({
                      classes,
                      InputProps: getInputProps({
                        placeholder: "Nombre",
                        name: "nombre"
                      })
                    })}
                    <div {...getMenuProps()}>
                      {isOpen ? (
                        <Paper className={classes.paper} square>
                          {getSuggestions({
                            value: inputValue,
                            suggestions: clientes,
                            key: "nombre"
                          }).map((suggestion, index) =>
                            renderSuggestion({
                              suggestion,
                              index,
                              itemProps: getItemProps({
                                item: suggestion.nombre
                              }),
                              highlightedIndex,
                              selectedItem,
                              key: "nombre"
                            })
                          )}
                        </Paper>
                      ) : null}
                    </div>
                  </div>
                )}
              </Downshift>

              <Downshift
                id="apellido"
                onChange={(selection, stateAndHelpers) =>
                  this.handleSelect(selection, stateAndHelpers)
                }
                inputValue={
                  this.state.selectedCliente
                    ? this.state.selectedCliente.apellido
                    : ""
                }
                onInputValueChange={this.onInputValueChange}
              >
                {({
                  getInputProps,
                  getItemProps,
                  getMenuProps,
                  highlightedIndex,
                  inputValue,
                  isOpen,
                  selectedItem
                }) => (
                  <div className={classes.container}>
                    {renderInput({
                      classes,
                      InputProps: getInputProps({
                        placeholder: "Apellido",
                        name: "apellido"
                      })
                    })}
                    <div {...getMenuProps()}>
                      {isOpen ? (
                        <Paper className={classes.paper} square>
                          {getSuggestions({
                            value: inputValue,
                            suggestions: clientes,
                            key: "apellido"
                          }).map((suggestion, index) =>
                            renderSuggestion({
                              suggestion,
                              index,
                              itemProps: getItemProps({
                                item: suggestion.apellido
                              }),
                              highlightedIndex,
                              selectedItem,
                              key: "apellido"
                            })
                          )}
                        </Paper>
                      ) : null}
                    </div>
                  </div>
                )}
              </Downshift>

              <Downshift
                id="email"
                onChange={(selection, stateAndHelpers) =>
                  this.handleSelect(selection, stateAndHelpers)
                }
                inputValue={
                  this.state.selectedCliente
                    ? this.state.selectedCliente.email
                    : ""
                }
                onInputValueChange={this.onInputValueChange}
              >
                {({
                  getInputProps,
                  getItemProps,
                  getMenuProps,
                  highlightedIndex,
                  inputValue,
                  isOpen,
                  selectedItem
                }) => (
                  <div className={classes.container}>
                    {renderInput({
                      classes,
                      InputProps: getInputProps({
                        placeholder: "Email",
                        name: "email"
                      })
                    })}
                    <div {...getMenuProps()}>
                      {isOpen ? (
                        <Paper className={classes.paper} square>
                          {getSuggestions({
                            value: inputValue,
                            suggestions: clientes,
                            key: "email"
                          }).map((suggestion, index) =>
                            renderSuggestion({
                              suggestion,
                              index,
                              itemProps: getItemProps({
                                item: suggestion.email
                              }),
                              highlightedIndex,
                              selectedItem,
                              key: "email"
                            })
                          )}
                        </Paper>
                      ) : null}
                    </div>
                  </div>
                )}
              </Downshift>
            </div>
          </DialogContent>
          <DialogActions>
           
            <Button onClick={this.handleClose} color="primary" disabled={!this.state.selectedCliente.id}>
              Elegir
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

DialogCliente.propTypes = {
  classes: PropTypes.object.isRequired,
  onClose: PropTypes.func,
  selectedValue: PropTypes.string
};

function renderInput(inputProps) {
  const { InputProps, classes, ref, ...other } = inputProps;

  return (
    <TextField
      InputProps={{
        inputRef: ref,
        classes: {
          root: classes.inputRoot,
          input: classes.inputInput
        },
        ...InputProps
      }}
      {...other}
    />
  );
}

function renderSuggestion({
  suggestion,
  index,
  itemProps,
  highlightedIndex,
  selectedItem,
  key
}) {
  const isHighlighted = highlightedIndex === index;
  const isSelected = (selectedItem || "").indexOf(suggestion.label) > -1;

  return (
    <MenuItem
      {...itemProps}
      key={suggestion.nroDocumento}
      selected={isHighlighted}
      component="div"
      style={{
        fontWeight: isSelected ? 500 : 400
      }}
    >
      {suggestion[key]}
    </MenuItem>
  );
}

export default withStyles(styles)(DialogCliente);
