import React from "react";
import "./App.css";

import SimpleTabs from "./Tabs";

function App() {
  return <SimpleTabs />;
}

export default App;
