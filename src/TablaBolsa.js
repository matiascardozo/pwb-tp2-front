import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
});


function TablaBolsa(props) {
  const { classes, datos } = props;

  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell align="right">Cliente</TableCell>
            
            <TableCell align="right">Fecha de asignación</TableCell>
            <TableCell align="right">Fecha de caducidad</TableCell>
            <TableCell align="right">Puntaje Asignado</TableCell>
            <TableCell align="right">Puntaje Utilizado</TableCell>
            <TableCell align="right">Saldo</TableCell>
            <TableCell align="right">Monto de la operación original</TableCell>

          </TableRow>
        </TableHead>
        <TableBody>
          {datos.map(dato => (
            <TableRow key={dato.id}>
              
              <TableCell align="right">{`${dato.Cliente.nombre}  ${dato.Cliente.apellido}` }</TableCell>
              <TableCell align="right">{dato.fechaAsignacion}</TableCell>
              <TableCell align="right">{dato.fechaCaducidad}</TableCell>
              <TableCell align="right">{dato.puntajeAsignado}</TableCell>
              <TableCell align="right">{dato.puntajeUtilizado}</TableCell>
              <TableCell align="right">{dato.saldo}</TableCell>
              <TableCell align="right">{dato.montoOperacion}</TableCell>

            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
  );
}

TablaBolsa.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TablaBolsa);