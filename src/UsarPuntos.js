import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import axios from "axios";
import React from "react";
import DialogCliente from "./DialogCliente";
import TextField from "@material-ui/core/TextField";
import TablaUso from "./TablaUso";
import Snackbar from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";

const styles = theme => ({
  root: {
    display: "flex",
    flexWrap: "wrap"
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2
  },
  button: {
    margin: theme.spacing.unit
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200
  }
});

class ConsultaUso extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      clientes: [],
      error: false,
      loading: true,
      open: false,
      selectedCliente: "",
      conceptos: [],
      selectedConcepto: "",
      fechaInicio: "",
      fechaHastsa: "",
      usos: []
    };
  }

  componentDidMount() {
    this.getConceptos();
  }

  getConceptos = () => {
    axios
      .get("/concepto")
      .then(response => this.setState({ conceptos: response.data }))
      .catch(error => console.log(error));
  };

  handleClose = value => {
    axios.get("/bolsa", {
      params: {
      vigentes:"true",
      vencidos:"false",
      cliente: value.id,
      }
    }).then(response => {
      const total = response.data.reduce((prev, curr) => prev + curr.saldo, 0)
      this.setState({ selectedCliente: value, open: false, puntosDisponibles: total  });

    })
  };

  handleClickOpen = () => {
    this.setState({
      open: true
    });
  };

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleSubmit = event => {
    const {
      selectedCliente,
      selectedConcepto,
      fechaInicio,
      fechaFin
    } = this.state;
    axios
      .post("/uso", {
        clientId: selectedCliente.id,
        conceptoId: selectedConcepto
      })
      .then(response => this.setState({ usos: response.data }))
      .catch(error => {
        console.log(error);
        this.setState({
          error: true,
          message: error.response.data,
          openSnackbar: true
        });
      });
  };

  handleCloseSnackbar = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    this.setState({ open: false });
  };

  render() {
    const { clientes, error, loading, conceptos, usos, message, puntosDisponibles } = this.state;
    const { classes } = this.props;

    return (
      <React.Fragment>
        <Typography variant="subtitle1">
          Cliente seleccionado:{" "}
          {this.state.selectedCliente &&
            this.state.selectedCliente.nroDocumento}
        </Typography>
        {puntosDisponibles && <Typography variant="subtitle1">
          Puntos disponibles:{" "}
          {this.state.puntosDisponibles}
        </Typography>
        }
        <br />
        <Button
          variant="outlined"
          color="primary"
          onClick={this.handleClickOpen}
        >
          Seleccionar Cliente
        </Button>
        <DialogCliente
          selectedCliente={this.state.selectedCliente}
          open={this.state.open}
          onClose={this.handleClose}
          clientes={this.state.clientes}
        />
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="concepto">Concepto</InputLabel>
          <Select
            value={this.state.selectedConcepto}
            onChange={this.handleChange}
            inputProps={{
              name: "selectedConcepto",
              id: "concepto"
            }}
          >
            {conceptos.map(concepto => (
              <MenuItem key={concepto.id} value={concepto.id}>{concepto.descripcion}</MenuItem>
            ))}
          </Select>
        </FormControl>
        {this.state.selectedConcepto && (
          <Typography variant="subtitle2" gutterBottom>
            Costo:{" "}
            {
              this.state.conceptos.find(
                c => c.id == this.state.selectedConcepto
              ).puntosRequeridos
            }
          </Typography>
        )}
        <Button
          variant="contained"
          className={classes.button}
          onClick={this.handleSubmit}
        >
          Comprar
        </Button>
        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "left"
          }}
          open={this.state.openSnackbar}
          autoHideDuration={6000}
          onClose={this.handleCloseSnackbar}
          ContentProps={{
            "aria-describedby": "message-id"
          }}
          message={<span id="message-id">{message}</span>}
          action={[
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              className={classes.close}
              onClick={this.handleCloseSnackbar}
            >
              <CloseIcon />
            </IconButton>
          ]}
        />{" "}
        {usos && <TablaUso datos={usos} />}
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(ConsultaUso);
