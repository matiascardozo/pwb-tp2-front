import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import axios from "axios";
import React from "react";
import DialogCliente from "./DialogCliente";
import TextField from "@material-ui/core/TextField";
import TablaBolsa from "./TablaBolsa";
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

const styles = theme => ({
  root: {
    display: "flex",
    flexWrap: "wrap"
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2
  },
  button: {
    margin: theme.spacing.unit
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200
  }
});

class ConsultaUso extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      clientes: [],
      error: false,
      loading: true,
      open: false,
      selectedCliente: "",
      conceptos: [],
      selectedConcepto: "",
      fechaInicio: "",
      fechaHastsa: "",
      usos: [],
      vigentes: false,
      vencidos: false
    };
  }



  handleClose = value => {
    this.setState({ selectedCliente: value, open: false });
  };

  handleClickOpen = () => {
    this.setState({
      open: true
    });
  };

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleChangeCheckbox = name => event => {
    this.setState({ [name]: event.target.checked });
  };

  handleSubmit = event => {
    const {
      selectedCliente,
      vigentes,
      vencidos,
      vencimientos
     
    } = this.state;
    axios
      .get("/bolsa", {
        params: {
          cliente: selectedCliente.id,
          vigentes,
            vencidos,
            vencimientos
        }
      })
      .then(response => this.setState({ usos: response.data }))
      .catch(error => console.log(error));
  };

  render() {
    const { clientes, error, loading, conceptos, usos } = this.state;
    const { classes } = this.props;

    return (
      <React.Fragment>
        <Typography variant="subtitle1">
          Cliente seleccionado:{" "}
          {this.state.selectedCliente &&
            this.state.selectedCliente.nroDocumento}
        </Typography>
        <br />
        <Button
          variant="outlined"
          color="primary"
          onClick={this.handleClickOpen}
        >
          Seleccionar Cliente
        </Button>
        <DialogCliente
          selectedCliente={this.state.selectedCliente}
          open={this.state.open}
          onClose={this.handleClose}
          clientes={this.state.clientes}
        />
        <form className={classes.root} autoComplete="off">
          <FormGroup row>
            <FormControlLabel
              control={
                <Checkbox
                  checked={this.state.vencidos}
                  onChange={this.handleChangeCheckbox("vencidos")}
                  value="Vencidos"
                />
              }
              label="Vencidos"
            />

            <FormControlLabel
              control={
                <Checkbox
                  checked={this.state.vigentes}
                  onChange={this.handleChangeCheckbox("vigentes")}
                  value="checkedB"
                />
              }
              label="Vigentes"
            />
          </FormGroup>
          <FormControl className={classes.formControl}>
          <TextField
          id="standard-number"
          label="Vencimiento en: (días)"
          value={this.state.vencimientos}
          name="vencimientos"
          onChange={this.handleChange}
          type="number"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          margin="normal"
        />
          </FormControl>
          <Button
            variant="contained"
            className={classes.button}
            onClick={this.handleSubmit}
          >
            Buscar
          </Button>
        </form>

        <TablaBolsa datos={usos} />
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(ConsultaUso);
