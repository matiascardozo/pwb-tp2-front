import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import axios from "axios";
import React from "react";
import DialogCliente from "./DialogCliente";
import TextField from "@material-ui/core/TextField";
import TablaUso from "./TablaUso"

const styles = theme => ({
  root: {
    display: "flex",
    flexWrap: "wrap"
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2
  },
  button: {
    margin: theme.spacing.unit
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200
  }
});

class ConsultaUso extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      clientes: [],
      error: false,
      loading: true,
      open: false,
      selectedCliente: "",
      conceptos: [],
      selectedConcepto: "",
      fechaInicio: "",
      fechaHastsa: "",
      usos:[]
    };
  }

  componentDidMount() {
    this.getConceptos();
  }

  getConceptos = () => {
    axios
      .get("/concepto")
      .then(response => this.setState({ conceptos: response.data }))
      .catch(error => console.log(error));
  };

  handleClose = value => {
    this.setState({ selectedCliente: value, open: false });
  };

  handleClickOpen = () => {
    this.setState({
      open: true
    });
  };


 


  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleSubmit = event => {
    const {
      selectedCliente,
      selectedConcepto,
      fechaInicio,
      fechaFin
    } = this.state;
    axios.get("/uso", {
      params: {
        cliente: selectedCliente.id,
        concepto: selectedConcepto,
        fechaInicio,
        fechaFin
      }
    }).then(response => this.setState({usos:response.data} )).catch(error => console.log(error)) ;
  };

  render() {
    const { clientes, error, loading, conceptos,usos } = this.state;
    const { classes } = this.props;

    return (
      <React.Fragment>
        <Typography variant="subtitle1">
          Cliente seleccionado:{" "}
          {this.state.selectedCliente && this.state.selectedCliente.nroDocumento}
        </Typography>
        <br />
        <Button
          variant="outlined"
          color="primary"
          onClick={this.handleClickOpen}
        >
          Seleccionar Cliente
        </Button>
        <DialogCliente
          selectedCliente={this.state.selectedCliente}
          open={this.state.open}
          onClose={this.handleClose}
          clientes={this.state.clientes}
        />
        <form className={classes.root} autoComplete="off">
          <FormControl className={classes.formControl}>
            <InputLabel htmlFor="concepto">Concepto</InputLabel>
            <Select
              value={this.state.selectedConcepto}
              onChange={this.handleChange}
              inputProps={{
                name: "selectedConcepto",
                id: "concepto"
              }}
            >
              {conceptos.map(concepto => (
                <MenuItem  key={concepto.id} value={concepto.id}>{concepto.descripcion}</MenuItem>
              ))}
            </Select>
          </FormControl>
          <FormControl className={classes.formControl}>
            <TextField
              id="fechaInicio"
              label="Fecha inicio"
              type="date"
              className={classes.textField}
              InputLabelProps={{
                shrink: true
              }}
              onChange={this.handleChange}
              name='fechaInicio'
            />
          </FormControl>

          <FormControl className={classes.formControl}>
            <TextField
              id="fechaFin"
              label="Fecha fin"
              type="date"
              className={classes.textField}
              InputLabelProps={{
                shrink: true
              }}
              onChange={this.handleChange}
              name='fechaFin'



            />
          </FormControl>
          <Button
            variant="contained"
            className={classes.button}
            onClick={this.handleSubmit}
          >
            Buscar
          </Button>
        </form>

              <TablaUso datos={usos}/>
            
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(ConsultaUso);
